
![Strikr Website Project](../../img/strikr_website.png)

##### Strikr Website Project

Strikr website is the central place for all published information which includes

- documentation
- books
- project information
- contributor guide
- governance
- reference info.

The content published to the subdomain [ref.strikr.io](https://ref.strikr.io) falls within the ambit of the Strikr website project.


##### Git repo

- [codeberg.org/strikr/strikr_website](https://codeberg.org/strikr/strikr_website)



##### Help Wanted

All the authored content is written in markdown.

We need help with translating the content to multiple languages. Please see the discussion thread at

- https://www.mail-archive.com/foss@strikr.io/msg00513.html


##### Reference

- [Markdown](https://daringfireball.net/projects/markdown/)
