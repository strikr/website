
![Strikr oneCLI Project](../../img/strikr_onecli.png)

##### Strikr oneCLI project

oneCLI is an experiment to create a uniform specification and implementation of the command-line interface (CLI) across all our free software projects.

A consistent command-line interface has the potential to unlock productivity through automation and intelligence unlike a graphical user interface (GUI) which is limited by design. We will never waste time developing GUI since it will get subsumed by machine learning (ML) anyways. Instead if anything, we apply ML to oneCLI framework so that it can operate in assistive and predictive mode as well.


##### Legal

- Specification            [GFDL3-or-later](../../governance/license/gfdlv3.md)
- Reference Implementation [AGPLv3-or-later](../../governance/license/agplv3.md)


##### Git repo

- [codeberg.org/strikr/strikr_onecli](https://codeberg.org/strikr/strikr_onecli)


##### References

- [Command Line Interface](https://en.wikipedia.org/wiki/Command-line_interface)


