
![Strikr C++ Logo](../../img/cc_book.png)

##### Modern C++20 Programming Book

This C++20 Programming book along with the illustrations and the source code is released under [GNU Free Documentation License v1.3 or later](../../governance/license/gfdlv3.md). All other sources of information including but not limited to videos, tweets, mailing lists, bugs, blog posts, schematic diagrams, relevant sections of the ISO C++ draft, WG21 mailings and papers are attributed in a *separate attribution section* at the end of each page with URL links. We donot refer to, use, adapt or attribute to any proprietary information anywhere to the maximum extent possible.


##### Table of Contents

* [Reserved Keywords and Identifiers](keywords.md)
* [Operator Precendence Table](operator.md)

