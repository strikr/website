
![GNU GCC](../img/cc_gnu_gcc.png)

```g++``` is the compiler of choice for all modern ```C++20/23``` software engineering along with ```libstdc++``` at Strikr free software project.

In order to ensure that we are able to use the latest C++ *features and bug-fixes* we custom compile the GCC source code.


##### Overview

Key points to note about the *custom* build

* the target is always CPU=amd64 LIBC=glibc VENDOR=linux with AMD64 specific optimizations
* only two languages are compiled ```C``` and ```C++```. this is by design and will not change.
* we build ```isl``` directly from its git repo instead of a specific version (>= 0.23)
* the upstream project GCC ```initial-branch``` is built on a weekly basis


##### Download

You are welcome to download the latest GCC 12.0.0 20210721 *experimental* build from
[https://sourceforge.net/projects/strikr/files/gcc/](https://sourceforge.net/projects/strikr/files/gcc/)


##### Help

Need assistance with modern C++20/23 programming or want to discuss how to use the GCC toolchain, please share your thoughts with the community by writing to [foss@strikr.io](mailto:foss@strikr.io) mailing list.
