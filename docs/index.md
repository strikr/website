
Remove Inequality by Production of Knowledge.
<br />
إزالة اللامساواة من خلال إنتاج المعرفة.
<br />
通过生产知识消除不平等。

<img src=./img/strikr.png width=100 height=100>


Strikr Free Software Project is a **community-centric** initiative to produce **infrastructure software**.

Infrastructure software is the foundation upon which the digital and technological capabilities of a modern society are built.


![AGPLv3](./img/agplv3.png)


**Software freedom** is not only **essential** but also **non-negotiable** to ensure choice and equal access to all individuals without coercion, encumberance, embargo or export controls.

We produce [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html) licensed [free software](https://www.gnu.org/philosophy/open-source-misses-the-point.en.html) for a network of individuals that operate with an enterprise or institutions such as universities, research labs and individuals committed to advancing the goals of software freedom for the society at large.


![strikr triplet GNULinux - Cplusplus - AGPLv3](./img/triplet.png)


We know it is not easy but it **is worth it**. As we work towards our goal, we take pride in knowing that *our struggle will play the biggest role in our purpose* (line inspired by the Rashford mural created by [Akse](https://www.greatermancunians.blog/akse-p19-street-artist-manchester), a graffiti artist)

*Join* the community today to help advance the cause of software freedom for a benign future.

We can be **altruistic** and **successful** !

![I Love C++](./img/ilovecplusplus.png)