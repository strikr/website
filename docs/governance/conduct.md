
![Strikr code of conduct](../img/tux_confused.png)


hackathon
online
mailing list
IRC
face2face
participating at events/sessions organized by other communities or institutions.


strikr code of conduct (SCC)

aka

* aparmaq
* conducta
* طرز عمل
* conduite
* 执行
* هدایت
* Verhalten
* kelakuan
* إجراء
* ponašanje
* condotta
* आचरण
* mengadakan
* anshax marin
* akhlaaq


SCC is "in force" at all Strikr organized interactions and is "enforced" at all Strikr sessions while participating at events/sessions organized by other communities or institutions.

eg. FOSDEM has its own of conduct, however the room in which Strikr presents, all the attendees, coordinators including technicians are subject to strikr code of conduct. Prior to start of the session everyone will be made aware of the Strikr code of conduct. Any violation(s) of Strikr code of conduct will be dealt with immediate affect as per Strikr governance model.



Rewards and Recognition (RAR)


Meetings

* attendee(s) will be individuals who are required to attend.


Communities are susceptible to saboteurs. Hence detection and weeding off such individuals or foundation 'front's will always be a ongoing activity.


Observation

* flame bait throwers, traitors, mercenaries, eavesdropper(s) will be identified and their recording will be initiated on their behaviour covertly. They will be encouraged further so that they amplify their behaviour. support team will collect all their publicly available information including photographs, video recordings, written communication, places visited etc.



Tools to use

* Reason is often a much sharper sword than anger.
* look for inconsistencies and fabrications in the narrative
* inversion technique



Tricks effective 2021

* corporates will attack free software through deception, dirty tricks and XYZ foundation_fronts.
* tactical rumour mongering, cancel culture etc
* our weapon is our minds and execution skills
* our strength is our decentralized collective


Talent

* find white hair in white milk

* take the side of the righteous not the powerful.

* strong sense of justice


* If your intentions are good, our future shall be too !

Unity, Strength, Order.

The root of everything is a family.














----
We are a online community from day one. For benefit of all the participants, we request all the members, organizers, sponsors, contributors to follow the community code of conduct. 


### Expected Behavior

* No recordings of any of the sessions is allowed
* Be considerate, respectful and collaborative towards each other
* Refrain from demeaning, discriminatory or harassing behavior and speech.
* No hate speech, comparison between entities is allowed
* Attendees and Organizers are requested to keep the context of the meet alive in the all conversations and limit themselves to it
* Sharing or asking for personal information (Location, Age, gender, salary, work status, marital status) is not allowed
* All questions related to community focus should be asked on the mailing list : foss@strikr.io
* Negative or Hate speech against other groups is not allowed


### Unacceptable behavior

* Stalking
* Usage of offensive words related to gender, sexual orientation, race, religion, disability
* virtual photography or recording
* disruption of talks
* sharing of inappropriate images or videos
* commenting irresponsible and out of context
* showcasing bad intention towards a particular human.


### Consequences thereof

Unacceptable behavior will not be tolerated by attendees or organizers. Immediate action would be taken against the party. such as:

* Blocking the person from all the community events
* making sure other communities are aware such behavior
* publicly disclosing the person's behavior to make sure everyone is aware about the intention of the person

We as a Free Software community expect all the participants to abide by the code of conduct.


### Seek Help

If any of our community member or organizer witnesses or are at the receiving end of the bad behavior, we request you to contact us immediately in strict confidence by writing to [ragini@strikr.io](mailto:ragini@strikr.io)

If the behavior leads to mental harrasement or physical abuse, we request you to immediately report the incident to the community and the local police in your area.


<hr/>

art credit: armored penguin.
