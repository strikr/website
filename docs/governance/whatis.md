
![Strikr tux group](../img/tux_group.png)


A inclusive organization requires an elightened model of governance to support and sustain active participation of community members and not coterie of board of directors or share holders who call the shots.

It can be scary for many people to think about a democratic organization given what they experience in their jobs and workplaces. At Strikr we have taken a conscious decision to think for ourselves as a community and how we can make a significant impact and influence over the next half a century. The first step in that direction is governance !


##### What is Governance ?

Governance is essentially about

* How do we execute on our goals ?
* How do we communicate ?
* How do we make decisions ?
* How do we build the framework for situational leadership ?
* How do we hold ourselves accountable ?
* How do we undertake corrective action ?


![Strikr tux justice](../img/tux_justice.png)

##### Justice

"Justice" and "Meritocracy" are an integral part of our traditions. We also view 'Meritocracy' as a principle of 'Justice'. As a result traditions are important to the delivery of governance. 


In order to achieve the *goals* we have set for ourselves, we need a just, universal and resilient model of governance that applies to our community members world-wide and that which we take great pride in living by. As a bonus, it will automatically filter out individuals who donot identify with our vision and help us identify individuals who outwardly seem to collaborate with us but have nefarious designs or primarily seek to distract the community from addressing the issues where energy and effort should be.


<hr/>

art credit: tux justice logo copyright and released to public domain by Larry Ewing, Simon Budig and Anja Gerwinski.
