
![Strikr Leadership](../img/tux_leadership.png)

##### Leadership

"Leadership is a process of social influence, which maximizes the efforts of others, towards the achievement of a goal" - Kevin Kruse.

The world of Free Software demands **situational leadership** and our most important imperative is to **get things done** !

In particular at Strikr Free Software Community we outline leadership across two overlapping roles, **Facilitator** and **Contributor**. We further qualify each role in terms of task specialization.

Leadership role(s)

 * Facilitator
 * Contributor

Task Specialization(s)

 * Oversight
 * Press Relations
 * Fund Raising
 * Liason
 * Legal
 * Auditor
 * License
 * Accounts
 * Finance
 * Loans
 * Onboarding
 * Offboarding
 * Ombudsman
 * Code Audit
 * Documentation
 * Code Review
 * License Review
 * University
 * First Contribution
 * Lead user
 * Maintainer
 * Patch
 * Release
 * Merge
 * Triage
 * Package
 * Blog
 * Tweet
 * Vulnerability Assessment
etc.

What makes our community unique is this synergy of facilitator-contributor duality premised on our tradition of **justice** and **meritocracy**. It is an extension of our belief that **economics** and **justice** are intimately related.

For our community to grow, we sharpen the focus and strengthen the two overlapping circles.

In this journey, we *seek the companionship* of those who believe in our vision and walk with us side by side on the road marked by milestones of achievement and sometimes a setback.


<hr />
image credit: Larry Ewing, tux mascot, 1996.