
![Strikr tux dance](../img/tux_dance.png)

##### Values

Values are an ensemble of principles and beliefs that relate to what is important, what is worthwhile and where we are invested emotionally. Values operate sub-consciously in our mind and influence our behaviour.

* We believe that economics and justice are intimately related and underwritten by community. In other words, a economy is powered by both capital and social capital.
* We have a singular *focus* towards production of free software components, libraries, framework and documentation.
* We nucleate, nurture a community of individuals who have the passion to dedicate themselves towards serving a higher cause of equality.
* We *attract* the individuals who align with our vision and we actively *repel* the individuals who do not align with our vision or are merely free loaders.
* We identify community stewards by their skin in the game.
* We take a unorthodox legacy-free approach to technology.
* We have a *can do* attitude and we help each other succeed.
* We know that the entire world is our oyster.
* We operate with a sense of urgency !


<hr>

art credit: public domain file. need help to locate the author and attribute the copyright. if you know, please write to legal@strikr.io