
![Strikr Logo](../img/logo_strikr.png)

##### Strikr Domain name

The .IO (indian ocean) domain, ```strikr.io``` is a domain registered by project co-founder, Ragini Jain, in year 2017.

Ragini Jain continues to be the rightful owner of the domain. She has not leased, sold or relinquished any of her ownership rights on strikr.io domain.

Consistent with ICANN registry requirements, Ragini is the administrative, technical, legal and billing contact for the strikr.io domain.

For any further queries, please write to [legal@strikr.io](mailto:legal@strikr.io)


