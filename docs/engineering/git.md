
![Git SCM](../img/git.png)

We use Git to version, track, archive and share all artifacts.


##### initial-branch

```git init --initial-branch=strikr```

what is usually referred to as ```main``` or ```master``` is what we refer to as ```strikr``` branch when discussing about Strikr projects.


##### git workflow

![strikr git workflow](../img/git_workflow.png)

* strikr project
    - we *always* work on our ```feature``` or ```personal``` branch
    - we *pull* updates from the ```strikr``` branch and ```rebase``` if required
    - we *create* a pull request for the change that we want to see merged in the 'strikr' branch

* upstream project:
    - we *track* and *pull* change of the *upstream* project(s) from their ```initial-branch```
    - we ```rebase``` the changes.
    - contributing patches
        - we create patch or a bug fix on a separate branch
        - we *contribute* patches as a *pull request* to the *upstream* project.


##### release

* we keep code at production quality
* we continuously update the test cases
* we are always ready to ship


##### updates

* we communicate the project plans without over-promising.


<hr />

image credit: Git Orange logomark for light backgrounds by Json Long at [Git website](https://git-scm.com/downloads/logos).