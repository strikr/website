Terms that commonly show up as part of our community conversations.

#####   A

- ABC Always Be Curious
- AFS Astonishment Follows Stupidity
- AND Abilities not Desires
- ASS Always Speak Sense

#####  B 

- BTL Be The Leader
- BRO BRutal hOnesty

#####  C 

- CAB Colony and Bazaar
- CBT Consistency Beats Talent
- CIS Cultural & Institutional Support
- CTN Change The Narrative

#####  D 

- DIV Diplomacy is Vindictive
- DODG (the smart can figure this out)

#####  E 

#####  F 

- FTC From Their Colony

#####  G

- GDP Geographic Diversity of Participants

#####  H

- HSR High Self Respect

#####  I

#####  J

#####  K

- KTF Kick the Fool

#####  L

- LOC Lack of Conditioning

#####  M

- MAFANGO   (the smart can figure this out)
- MAFANGOIS
- MSD Marketing Stunt in Disguise

#####  N

- Noor-Nune-Nifo (to be used instead of foo-bar-baz)
- NBC Never be Complacent

#####  O

#####  P

- PAF Patience and Focus
- PEP Passion Enthusiasm Persistence
- PFP People from Past
- POS Position of Strength
- PSX Positive Support Experience

#####  Q

#####  R

- RAT Radical Transparency
- RCS Random Cool Stack
- RTS Ready To Ship

#####  S

- SMS Something More Significant
- SOP Smartness on Paper
- SRM Stable Reliable Maintenable

#####  T

- TAM They Are Many
- TAR Types Algorithms and Relationships
- TRC Thinking Requires Calmness
- TFI Talent For Innovation
- TFM Talent For Imitation
- TWT Time Wasting Tactic

#####  U

- UDA Up Down Across

#####  V

#####  W

- WADR With All Due Respect

#####  X

- XAC 'X' as Code

#####  Y

- YFC Your First Contribution

#####  Z


