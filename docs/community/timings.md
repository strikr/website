
![Strikr Timings](../img/tux_office.png)


All Strikr community interactions start at a fixed well-defined global time.

- [daily community hours](daily.md)
- [saturday meets](meet.md)
- [hackathons](hackathon.md)


##### Global

- start time *(UTC+0000)* **0400** hrs


##### City specific timings

As a courtesy we provide local timings for some cities.

- (UTC-03:00) 01:00 São Paulo
- (UTC+00:00) 04:00 ---- Event Baseline ----
- (UTC+00:00) 04:00 London (DST may apply)
- (UTC+01:00) 05:00 Algiers
- (UTC+01:00) 05:00 Berlin (DST may apply)
- (UTC+01:00) 05:00 Rabat
- (UTC+01:00) 05:00 Stockholm (DST may apply)
- (UTC+01:00) 05:00 Tunis
- (UTC+02:00) 06:00 Cairo
- (UTC+02:00) 06:00 Damascus
- (UTC+02:00) 06:00 Khartoum
- (UTC+03:00) 07:00 Ankara
- (UTC+04:00) 08:00 Dubai
- (UTC+05:00) 09:00 Islamabad
- (UTC+05:30) 09:30 New Delhi
- (UTC+06:00) 10:00 Dhaka
- (UTC+07:00) 11:00 Hanoi
- (UTC+07:00) 11:00 Jakarta
- (UTC+08:00) 12:00 Beijing
- (UTC+08:00) 12:00 Kuala Lampur
- (UTC+08:00) 12:00 Singapore
- (UTC+09:00) 13:00 Tokyo
- (UTC+09:00) 13:00 Sydney
- (UTC+13:00) 17:00 Auckland



##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.
