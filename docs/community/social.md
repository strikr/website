
![Strikr Social](../img/tux_social.png)

##### Social Media

We actively use [Mailing List](list.md), [IRC](irc.md) and [JITSI](jitsi.md) for [Daily Community Hours](daily.md), [Saturday Meets](meet.md) and [Hackathons](hackathon.md).


##### FOSS platforms

| FOSS platform     | audience, purpose                     | URL                                                                         | 
|:------------------|:--------------------------------------|:----------------------------------------------------------------------------|
| codeberg          | source code, git repo                 | [https://codeberg.org/strikr](https://codeberg.org/strikr)                  |
| peertube          | videos, live stream                   | [https://tube.tchncs.de/video-channels/strikr/videos](https://tube.tchncs.de/video-channels/strikr/videos)  |
| jitsi             | real-time audio, screen sharing       | [https://meet.jit.si/strikr](https://meet.jit.si/strikr)                    |
| gnu social        | event updates, news, quips            | [https://bluex.im/strikr](https://bluex.im/strikr)                          |
| mastodon          | all event updates                     | [https://mastodon.social/@strikr](https://mastodon.social/@strikr)          |



##### non-free platforms

We have registered on various *non-free* social media platforms to retain *strikr* or *strikrio* userid as placeholder to avoid any misrepresentation. we recommend **not to use** them.


| non-free platform | audience, purpose                     | URL                                                                         |
|:------------------|:--------------------------------------|:----------------------------------------------------------------------------|
| bitbucket         | source code                           |[https://bitbucket.org/strikr](https://bitbucket.org/strikr)               |
| eventbrite        | event registration, updates           |[https://strikr.eventbrite.com](https://strikr.eventbrite.com)               |
| fb Group          | students, faculty (discussions)       |[https://facebook.com/groups/strikr](https://facebook.com/groups/strikr)     |
| fb Page           | updates                               |[https://facebook.com/strikrIO](https://facebook.com/strikrIO)               |
| fb Events         | events and notifications              |[https://facebook.com/strikrIO/events](https://facebook.com/strikrIO/events) |
| github            | source code                           |[https://github.com/strikr](https://github.com/strikr)                       |
| instagram         | updates                               |[https://instagram.com/strikrIO](https://instagram.com/strikrIO)             |
| telegram          | updates, discussions                  |[https://t.me/strikrio](https://t.me/strikrio)                               |
| tumblr            | updates                               |[https://strikrio.tumblr.com](https://strikrio.tumblr.com)                   |
| twitch            | live stream, coding sessions          |[https://www.twitch.tv/strikrio](https://www.twitch.tv/strikrio)             |
| twitter           | updates, free software retweets       |[https://twitter.com/strikrio](https://twitter.com/strikrio)                 |
| youtube           | videos                                |[https://www.youtube.com/channel/UCRzdd-0c9FO1fGpSKCQ-Jng](https://www.youtube.com/channel/UCRzdd-0c9FO1fGpSKCQ-Jng)|

##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.