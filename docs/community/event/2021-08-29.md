
![PERL](../../img/perl.png)

##### PERL Mojolicious Real-time Web framework Applied

- date: 2021-08-29
- time: (UTC+0000) 0800 hrs
- type: Hackathon
- services
    - [#strikr](../irc.md)
    - [foss@strikr.io](../list.md)
    - [meet.jit.si/strikr](../jitsi.md)

##### Problem Statement

- implement a simple web application using Mojolicious web framework.
- collect a bunch of C++ function declarations as the input using a HTML form.
- generate the output of transformed C++ function declarations as per Strikr modern C++20/23 coding standard.
- implement the parsing and processing logic in a multi-module single-file PERL implementation.

Note

- Hands-on coding activity on GNU/Linux system
- Focus on modularity, interface and testing



<hr />
image credit: [The Perl Foundation](https://www.perlfoundation.org/)