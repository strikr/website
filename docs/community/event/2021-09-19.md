
![Emacs LISP](../../img/emacs.png)

##### GNU Emacs LISP

- date: 2021-09-19
- time: (UTC+0000) 0800 hrs
- type: WADR
- services
    - [#strikr](../irc.md)
    - [foss@strikr.io](../list.md)
    - [meet.jit.si/strikr](../jitsi.md)

##### Overview

Emacs LISP or ELisp is a dialect of LISP.

ELisp is used as a scripting language within the Emacs environment.

ELisp is used for implementing most of the editing functionality built into Emacs.

ELisp provides programmatic access to data-structures and functions that provide *structural editing* capabilities applied to the abstraction referred to as a *buffer* in Emacs.

We explore Emacs LISP in depth across three sessions.


##### Reference

- [Emacs LISP](https://en.wikipedia.org/wiki/Emacs_Lisp)
- [Emacs Lisp Limitations](https://www.emacswiki.org/emacs/EmacsLispLimitations)
- Robert J Chasell, An introduction to programming in Emacs LISP. (entire book)
- Harley Hahn, Emacs Field Guide. (Chap 11)
- Micket Petersen, Mastering Emacs. (Chap 2, 6)
- GNU Emacs Lisp Reference Manual (ver 28.0.50)
- Chris Done, [A quick guide to Emacs Lisp programming](https://github.com/chrisdone/elisp-guide)



<hr />
image credit: [The Perl Foundation](https://www.perlfoundation.org/)
