![PERL](../../img/perl.png)

##### PERL Regular Expressions

- date: 2021-08-21
- time: (UTC+0000) 0800 hrs
- type: saturday meet

##### Overview

Perl is widely renowned for excellence in text processing, and regular expressions are one of the big factors behind this fame.

Perl regular expressions display an efficiency and flexibility unknown in most other computer languages. Mastering even the basics of regular expressions will allow you to manipulate text with surprising ease.

You are expected to be running a GNU/Linux or POSIX compliant environment for working on the code.

##### Further Reading

- [perlre](https://perldoc.perl.org/perlre)
- [binding operator](https://perldoc.perl.org/perlop#Binding-Operators)
- [regular expression tutorial](https://perldoc.perl.org/perlretut)



##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.


<hr />
image credit: [The Perl Foundation](https://www.perlfoundation.org/)
