
![Strikr With All Due Respect](../../img/wadr.png)

##### Issue with Issue Tracking systems

- date: 2021-07-25
- time: (UTC+0000) 0800 hrs
- type: [WADR](../wadr.md)

##### Overview

We want to use a issue tracker for our project which is not hosted at some third party site.

we want to track items such as

- defect
- action item
- artifact
- cve
among others

with a bit of exploration we found that the common issue trackers are

- RT
- Bugzilla
- Roundup

they are all nice but not so nice to be used at Strikr free software project.

Read the original [email post](https://www.mail-archive.com/foss@strikr.io/msg00530.html).


##### Strikr Think

- command line is the primary interface
- priority, severity
- product - component - module
- ability to establish a connection | link between a Git commit hash and an issue.
    - commit_hash --> issue
    - issue --> commit_hash
- integrate it using the same credentials used with Git.
- all tracker operations and complete bug|defect history available from the command line.
- integrate and operable from within GNU Emacs.
- submit data to ML engine for project metrics
    - elapsed days from report to confirmation
    - elapsed days from confirmation to resolution
    - arrival rates
    - sprint size estimate
- generate a report in markdown format with support for bootstrap4 tables.
    - add the report to mkdocs project nav for the next publish to web schedule.


##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.


<hr />
image credit: collage created from royalty free images in the public domain.