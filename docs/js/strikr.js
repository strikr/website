//
// Strikr Free Software Project
//

(function (document) {

    function convert_uml (class_name, converter, settings) {

        var charts = document.querySelectorAll ("code." + class_name);
        var arr = [];
        var i;
        var j;
        var max_item;
        var diagram;
        var text;

        
        if (settings === void 0) {
            
            settings = {};
        }

        
        for (i = 0, max_item = charts.length; i < max_item; i++) {
            
            arr.push (charts[i]);
        }


        for (i = 0, max_item = arr.length; i < max_item; i++) {

            var pelem = arr[i].parentNode;

            text = arr[i].textContent || arr[i].innerText;

            if (arr[i].innerText) {
                arr[i].innerText = '';
            }
            else {
                arr[i].textContent = '';
            }

            var el = document.createElement ('div');

            el.className = class_name;

            pelem.parentNode.insertBefore (el, pelem);
            pelem.parentNode.removeChild  (pelem);

            diagram = converter.parse (text);
            diagram.drawSVG (el, settings);
        }
    }


    function onReady (fn) {
        if (document.addEventListener) {
            document.addEventListener ('DOMContentLoaded', fn);
        }
        else {
            document.attachEvent ('onreadystatechange', function () {
                if (document.readyState == 'interactive') {
                    fn ();
                }
            });
        }
    }


    onReady (function () {
        convert_uml ('language-uml-sequence-diagram'
                     , Diagram
                     , { theme: 'simple' }
                    );
    });


})(document);


if (document.readyState == "complete") {
    alert ("page is loaded");
}
