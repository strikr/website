#!/bin/sh

# ----------------------------------------------------------
# Strikr Free Software Project
# https://strikr.io/
# ----------------------------------------------------------

CONF_OUTPUT_DIR=/opt/www/output/

rm -r -v -f ${CONF_OUTPUT_DIR}/*

mkdocs build                        \
       --strict                     \
       --verbose                    \
       --use-directory-urls         \
       --clean                      \
       --config-file ../mkdocs.yml  \
       --site-dir    ${CONF_OUTPUT_DIR}


exit 0;



# STRIKR commentary
# 2021-07-18 updated
